/**
 * Created by aaron on 05/03/2017.
 */
import React, { Component } from 'react';

class GuessComponent extends Component {
    constructor() {
        super();
        this.state = {
            gameState: 'ready',
            answer: null,
            guessValue: null,
            isRight: false
        };
        this.startGame = this.startGame.bind(this);
        this.handleValue = this.handleValue.bind(this);
        this.replayGame = this.replayGame.bind(this);
    }


    startGame() {
        let random = Math.round(Math.random()*100);
        this.setState({gameState:'playing', answer: random, guessValue: 0, isRight: false});
    }

    handleValue(event) {
        let inputValue = event.target.value;
        if(inputValue == this.state.answer) {
            this.setState({gameState:'end', answer: this.state.answer, guessValue: inputValue, isRight: true});
        }else {
            this.setState({gameState: 'playing', answer: this.state.answer, guessValue: inputValue, isRight: false});
        }
    }

    replayGame() {
        this.setState({gameState: 'ready', answer: 0, guessValue: 0, isRight: false});
    }



    renderGame() {
        switch (this.state.gameState) {
            case 'ready':
                return (
                    <div className="play-button" onClick={this.startGame}>
                        <p>Click Start Guess Game</p>
                    </div>

                );
            case 'playing':
                return (
                    <div className="input" >
                        Guess round [1~100], Please input value: <input type="number" value={this.state.guessValue} onChange={this.handleValue} />
                        <p>{this.state.guessValue} {this.state.guessValue > this.state.answer ? '>' : '<'} answer </p>
                    </div>
                );
            case 'end':
                return (
                    <div className="play-again-button" onClick={this.replayGame}>
                        <p>Great! Answer is {this.state.answer} ! </p>
                        <p>Click Replay Game !</p>
                    </div>
                );
        }
    }

    render() {
        return (
            <div className="GuessGame">
                {this.renderGame()}
            </div>
        );
    }
}
export default GuessComponent;