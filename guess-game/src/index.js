import React from 'react';
import ReactDOM from 'react-dom';
import GuessComponent from './guess/GuessComponent';
import './index.css';

ReactDOM.render(
  <GuessComponent />,

  document.getElementById('root')
);
